#include <iostream>
#include <vector>
#include <algorithm>
#include <random>

int main()
{
    // Initializing variables
    int deck[52];
    int deck_size = sizeof(deck) / sizeof(deck[0]);
    int i;
    auto rng = std::default_random_engine {};
    srand(time(0));
    // Generating deck
    for(i=0; i<52; i++)
    {
        deck[i] = i;
    }
    // Shuffling deck
    for(int i=0; i<52; i++)
    {
        int j = rand() % 52;
        std::swap(deck[i], deck[j]);
    }
    // Initializing more stuff
    std::string suit_values[4]={"Spades", "Diamons", "Clubs", "Hearths"};
    std::string card_values[13]={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
    int suit_values_size = sizeof(suit_values)/sizeof(suit_values[0]);
    int card_values_size = sizeof(card_values)/sizeof(card_values[0]);
    std::vector<int> deck_vector(deck, deck+deck_size);
    std::vector<std::string> suit_vector(suit_values, suit_values+suit_values_size);
    std::vector<std::string> card_vector(card_values, card_values+card_values_size);
    std::vector<int> player1_start_deck;
    std::vector<int> player2_start_deck;
    // Drawing deck to two players
    for (int i= 0; i < 52; i++)
    {
        if(i % 2)
        {
            player1_start_deck.push_back(deck[i]);
        }
        else
        {
            player2_start_deck.push_back(deck[i]);
        }
    }
    
    std::vector<int> player1_start_cards;
    std::vector<int> player1_start_suits;
    std::vector<int> player2_start_cards;
    std::vector<int> player2_start_suits;
    // Changing values to card values and suits
    for(int i= 0; i < player1_start_deck.size(); i++)
    {
        player1_start_cards.push_back(player1_start_deck[i]%13);
        player1_start_suits.push_back(player1_start_deck[i]/13);
    }
    for(int i= 0; i < player2_start_deck.size(); i++)
    {
        player2_start_cards.push_back(player2_start_deck[i]%13);
        player2_start_suits.push_back(player2_start_deck[i]/13);
    }
    // While loop for options
    while(1)
    {
        std::cout << "Please select if you want to play another round or concede" << std::endl;
        std::cout << "1 for another round"  << std::endl;
        std::cout << "2 for player1 condece" << std::endl;
        std::cout << "3 for player2 condede" << std::endl;
        std::cout << "4 for showing decks" << std::endl;
        std::cout << "5 for speedrun the game" << std::endl;
        int choice;
        std::cin >> choice;
        switch (choice)
        {
            case 1:
                    // If deck is empty
                    if(player1_start_cards.size() == 0)
                    {
                        std::cout << "player2 won this game"<< "\n";
                        return 0;
                    }
                    // If deck is empty
                    else if(player2_start_cards.size() == 0)
                    {
                        std::cout << "player1 won this game"<< "\n";
                        return 0;
                    }
                    // If player1 card is higher then it gets the card
                    else if(player1_start_cards.front() > player2_start_cards.front())
                    {
                        std::cout <<"player1 " << card_values[player1_start_cards[0]] << " of " << suit_values[player1_start_suits[0]]<< "\n";
                        std::cout << "against" << std::endl;
                        std::cout <<"player2 " << card_values[player2_start_cards[0]] << " of " << suit_values[player2_start_suits[0]]<< "\n";
                        std::cout << "Player1 won this pair" << std::endl;
                        
                        // Inserting own card and other player card to bottom of your deck and then removing them from top of your deck
                        player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+1);
                        player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+1);

                        player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+1);
                        player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+1);

                        player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+1);
                        player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+1);

                        player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+1);
                        player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+1);

                        std::cout << "" << std::endl;
                        std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                        std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                        
                    }
                    // If player2 card is higher then it gets the card
                    else if(player1_start_cards.front() < player2_start_cards.front())
                    {
                        std::cout <<"player1 " << card_values[player1_start_cards[0]] << " of " << suit_values[player1_start_suits[0]]<< "\n";
                        std::cout << "against" << std::endl;
                        std::cout <<"player2 " << card_values[player2_start_cards[0]] << " of " << suit_values[player2_start_suits[0]]<< "\n";
                        std::cout << "Player2 won this pair" << std::endl;
                        // Inserting own card and other player card to bottom of your deck and then removing them from top of your deck
                        player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+1);
                        player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+1);

                        player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+1);
                        player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+1);

                        player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+1);
                        player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+1);

                        player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+1);
                        player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+1);

                        std::cout << "" << std::endl;
                        std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                        std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                    }
                    // If both cards have same value then happens battle where next three cards are put on table and fifth cards fights and winner gets all if player have enough cards at the moment
                    else if(player1_start_cards.front() == player2_start_cards.front())
                    {
                        std::cout <<"player1 " << card_values[player1_start_cards[0]] << " of " << suit_values[player1_start_suits[0]]<< "\n";
                        std::cout << "against" << std::endl;
                        std::cout <<"player2 " << card_values[player2_start_cards[0]] << " of " << suit_values[player2_start_suits[0]]<< "\n";
                        std::cout << "Battle begins" << std::endl;
                        std::cout << "" << std::endl;
                        std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                        std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                        if((player1_start_deck.size() > 5 && player2_start_deck.size() > 5))
                        {
                            if(player1_start_cards.at(4) > player2_start_cards.at(4))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[4]] << " of " << suit_values[player1_start_suits[4]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[4]] << " of " << suit_values[player2_start_suits[4]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+5);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+5);
                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(4) < player2_start_cards.at(4))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[4]] << " of " << suit_values[player1_start_suits[4]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[4]] << " of " << suit_values[player2_start_suits[4]]<< "\n";
                                std::cout << "Player2 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+5);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+5);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(4) == player2_start_cards.at(4))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        else if(player1_start_deck.size() == 5)
                        {
                            if(player1_start_cards.back() > player2_start_cards.at(4))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[4]] << " of " << suit_values[player1_start_suits[4]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[4]] << " of " << suit_values[player2_start_suits[4]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+5);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+5);
                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(4))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                        
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(4))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        else if(player2_start_deck.size() == 5)
                        {
                            if(player1_start_cards.at(4) < player2_start_cards.back())
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[4]] << " of " << suit_values[player1_start_suits[4]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[4]] << " of " << suit_values[player2_start_suits[4]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+5);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+5);
                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(4) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                        
                            }
                            else if(player1_start_cards.at(4) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        else if(player1_start_cards.size() == 4 )
                        {
                            if(player1_start_cards.back() > player2_start_cards.at(3))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[3]] << " of " << suit_values[player1_start_suits[3]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[3]] << " of " << suit_values[player2_start_suits[3]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+4);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+4);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+4);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+4);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+4);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+4);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+4);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+4);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(3))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(3))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player2_start_cards.size() == 4 )
                        {
                            if(player1_start_cards.at(3) < player2_start_cards.back())
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[3]] << " of " << suit_values[player1_start_suits[3]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[3]] << " of " << suit_values[player2_start_suits[3]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+4);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+4);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+4);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+4);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+4);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+4);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+4);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+4);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(3) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.at(3) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player1_start_cards.size() == 3)
                        {
                            if((player1_start_cards.back() > player2_start_cards.at(2)))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[2]] << " of " << suit_values[player1_start_suits[2]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[2]] << " of " << suit_values[player2_start_suits[2]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+3);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+3);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+3);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+3);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+3);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+3);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+3);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+3);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(2))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(2))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player2_start_cards.size() == 3)
                        {
                            if((player1_start_cards.at(2) < player2_start_cards.back()))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[2]] << " of " << suit_values[player1_start_suits[2]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[2]] << " of " << suit_values[player2_start_suits[2]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+3);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+3);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+3);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+3);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+3);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+3);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+3);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+3);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(2) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.at(2) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player1_start_cards.size() == 2)
                        {
                            if(player1_start_cards.back() > player2_start_cards.at(1))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[1]] << " of " << suit_values[player1_start_suits[1]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[1]] << " of " << suit_values[player2_start_suits[1]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+2);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+2);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+2);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+2);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+2);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+2);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+2);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+2);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(1))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(1))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player2_start_cards.size() == 2)
                        {
                            if(player1_start_cards.at(1) < player2_start_cards.back())
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[1]] << " of " << suit_values[player1_start_suits[1]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[1]] << " of " << suit_values[player2_start_suits[1]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+2);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+2);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+2);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+2);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+2);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+2);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+2);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+2);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(1) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.at(1) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }

                        else if(player1_start_cards.size() == 1 )
                        {
                            if(player1_start_cards.back() > player2_start_cards.at(0))
                            {
                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+1);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+1);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+1);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+1);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+1);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+1);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+1);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+1);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(0))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(0))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        else if(player2_start_cards.size() == 1 )
                        {
                            if(player1_start_cards.at(0) < player2_start_cards.back())
                            {
                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+1);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+1);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+1);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+1);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+1);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+1);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+1);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+1);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(0) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.at(0) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        
                        if(player1_start_cards.size() == 0)
                        {
                            std::cout << "player2 won this game"<< "\n";
                            return 0;
                        }
                        if(player2_start_cards.size() == 0)
                        {
                            std::cout << "player1 won this game"<< "\n";
                            return 0;
                        }
                        
                    
                    }
                break;
            case 2:
                std::cout <<"player1 concedes" << "\n";
                return 0;

            case 3:
                std::cout <<"player2 concedes" << "\n";
                return 0;
            case 4:
                std::cout << "Player1 cards:" << std::endl;
                for(int i= 0; i < player1_start_cards.size(); i++)
                {
                    std::cout << card_values[player1_start_cards[i]] << " of " << suit_values[player1_start_suits[i]]<< "\n";
                }
                std::cout << "" << std::endl;
                std::cout << "Player2 cards:" << std::endl;
                for(int i= 0; i < player2_start_cards.size(); i++)
                {
                    std::cout << card_values[player2_start_cards[i]] << " of " << suit_values[player2_start_suits[i]]<< "\n";
                }
                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                break;
            
            case 5:
                while(1)
                {
                    if(player1_start_cards.size() == 0)
                    {
                        std::cout << "player2 won this game"<< "\n";
                        return 0;
                    }
                    else if(player2_start_cards.size() == 0)
                    {
                        std::cout << "player1 won this game"<< "\n";
                        return 0;
                    }
                    else if(player1_start_cards.front() > player2_start_cards.front())
                    {
                        std::cout <<"player1 " << card_values[player1_start_cards[0]] << " of " << suit_values[player1_start_suits[0]]<< "\n";
                        std::cout << "against" << std::endl;
                        std::cout <<"player2 " << card_values[player2_start_cards[0]] << " of " << suit_values[player2_start_suits[0]]<< "\n";
                        std::cout << "Player1 won this pair" << std::endl;
                        
                        
                        player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+1);
                        player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+1);

                        player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+1);
                        player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+1);

                        player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+1);
                        player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+1);

                        player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+1);
                        player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+1);

                        std::cout << "" << std::endl;
                        std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                        std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                        
                    }
                    else if(player1_start_cards.front() < player2_start_cards.front())
                    {
                        std::cout <<"player1 " << card_values[player1_start_cards[0]] << " of " << suit_values[player1_start_suits[0]]<< "\n";
                        std::cout << "against" << std::endl;
                        std::cout <<"player2 " << card_values[player2_start_cards[0]] << " of " << suit_values[player2_start_suits[0]]<< "\n";
                        std::cout << "Player2 won this pair" << std::endl;

                        player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+1);
                        player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+1);

                        player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+1);
                        player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+1);

                        player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+1);
                        player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+1);

                        player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+1);
                        player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+1);

                        std::cout << "" << std::endl;
                        std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                        std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                    }
                    else if(player1_start_cards.front() == player2_start_cards.front())
                    {
                        std::cout <<"player1 " << card_values[player1_start_cards[0]] << " of " << suit_values[player1_start_suits[0]]<< "\n";
                        std::cout << "against" << std::endl;
                        std::cout <<"player2 " << card_values[player2_start_cards[0]] << " of " << suit_values[player2_start_suits[0]]<< "\n";
                        std::cout << "Battle begins" << std::endl;
                        std::cout << "" << std::endl;
                        std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                        std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                        if((player1_start_deck.size() > 5 && player2_start_deck.size() > 5))
                        {
                            if(player1_start_cards.at(4) > player2_start_cards.at(4))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[4]] << " of " << suit_values[player1_start_suits[4]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[4]] << " of " << suit_values[player2_start_suits[4]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+5);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+5);
                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(4) < player2_start_cards.at(4))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[4]] << " of " << suit_values[player1_start_suits[4]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[4]] << " of " << suit_values[player2_start_suits[4]]<< "\n";
                                std::cout << "Player2 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+5);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+5);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(4) == player2_start_cards.at(4))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        else if(player1_start_deck.size() == 5)
                        {
                            if(player1_start_cards.back() > player2_start_cards.at(4))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[4]] << " of " << suit_values[player1_start_suits[4]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[4]] << " of " << suit_values[player2_start_suits[4]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+5);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+5);
                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(4))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                        
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(4))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        else if(player2_start_deck.size() == 5)
                        {
                            if(player1_start_cards.at(4) < player2_start_cards.back())
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[4]] << " of " << suit_values[player1_start_suits[4]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[4]] << " of " << suit_values[player2_start_suits[4]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+5);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+5);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+5);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+5);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+5);
                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(4) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                        
                            }
                            else if(player1_start_cards.at(4) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        else if(player1_start_cards.size() == 4 )
                        {
                            if(player1_start_cards.back() > player2_start_cards.at(3))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[3]] << " of " << suit_values[player1_start_suits[3]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[3]] << " of " << suit_values[player2_start_suits[3]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+4);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+4);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+4);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+4);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+4);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+4);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+4);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+4);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(3))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(3))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player2_start_cards.size() == 4 )
                        {
                            if(player1_start_cards.at(3) < player2_start_cards.back())
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[3]] << " of " << suit_values[player1_start_suits[3]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[3]] << " of " << suit_values[player2_start_suits[3]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+4);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+4);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+4);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+4);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+4);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+4);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+4);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+4);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(3) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.at(3) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player1_start_cards.size() == 3)
                        {
                            if((player1_start_cards.back() > player2_start_cards.at(2)))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[2]] << " of " << suit_values[player1_start_suits[2]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[2]] << " of " << suit_values[player2_start_suits[2]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+3);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+3);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+3);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+3);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+3);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+3);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+3);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+3);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(2))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(2))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player2_start_cards.size() == 3)
                        {
                            if((player1_start_cards.at(2) < player2_start_cards.back()))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[2]] << " of " << suit_values[player1_start_suits[2]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[2]] << " of " << suit_values[player2_start_suits[2]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+3);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+3);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+3);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+3);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+3);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+3);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+3);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+3);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(2) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.at(2) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player1_start_cards.size() == 2)
                        {
                            if(player1_start_cards.back() > player2_start_cards.at(1))
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[1]] << " of " << suit_values[player1_start_suits[1]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[1]] << " of " << suit_values[player2_start_suits[1]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+2);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+2);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+2);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+2);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+2);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+2);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+2);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+2);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(1))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(1))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }
                        else if(player2_start_cards.size() == 2)
                        {
                            if(player1_start_cards.at(1) < player2_start_cards.back())
                            {
                                std::cout <<"player1 " << card_values[player1_start_cards[1]] << " of " << suit_values[player1_start_suits[1]]<< "\n";
                                std::cout << "against" << std::endl;
                                std::cout <<"player2 " << card_values[player2_start_cards[1]] << " of " << suit_values[player2_start_suits[1]]<< "\n";
                                std::cout << "Player1 won this battle" << std::endl;

                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+2);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+2);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+2);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+2);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+2);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+2);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+2);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+2);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(1) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.at(1) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }
                        }

                        else if(player1_start_cards.size() == 1 )
                        {
                            if(player1_start_cards.back() > player2_start_cards.at(0))
                            {
                                player1_start_cards.insert(player1_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+1);
                                player1_start_suits.insert(player1_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+1);

                                player1_start_cards.insert(player1_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+1);
                                player1_start_suits.insert(player1_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+1);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+1);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+1);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+1);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+1);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.back() < player2_start_cards.at(0))
                            {
                                std::cout << "player2 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.back() == player2_start_cards.at(0))
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        else if(player2_start_cards.size() == 1 )
                        {
                            if(player1_start_cards.at(0) < player2_start_cards.back())
                            {
                                player2_start_cards.insert(player2_start_cards.end(), player2_start_cards.begin(), player2_start_cards.begin()+1);
                                player2_start_suits.insert(player2_start_suits.end(), player2_start_suits.begin(), player2_start_suits.begin()+1);

                                player2_start_cards.insert(player2_start_cards.end(), player1_start_cards.begin(), player1_start_cards.begin()+1);
                                player2_start_suits.insert(player2_start_suits.end(), player1_start_suits.begin(), player1_start_suits.begin()+1);

                                player2_start_cards.erase(player2_start_cards.begin(), player2_start_cards.begin()+1);
                                player2_start_suits.erase(player2_start_suits.begin(), player2_start_suits.begin()+1);

                                player1_start_cards.erase(player1_start_cards.begin(), player1_start_cards.begin()+1);
                                player1_start_suits.erase(player1_start_suits.begin(), player1_start_suits.begin()+1);

                                std::cout << "" << std::endl;
                                std::cout << "player1 deck size " << player1_start_cards.size()<< "\n";
                                std::cout << "player2 deck size " << player2_start_cards.size()<< "\n";
                            }
                            else if(player1_start_cards.at(0) > player2_start_cards.back())
                            {
                                std::cout << "player1 won this game"<< "\n";
                                return 0;
                            }
                            else if(player1_start_cards.at(0) == player2_start_cards.back())
                            {
                                std::shuffle(std::begin(player1_start_cards), std::end(player1_start_cards), rng);
                                std::shuffle(std::begin(player2_start_cards), std::end(player2_start_cards), rng);
                            }

                        }
                        
                        if(player1_start_cards.size() == 0)
                        {
                            std::cout << "player2 won this game"<< "\n";
                            return 0;
                        }
                        if(player2_start_cards.size() == 0)
                        {
                            std::cout << "player1 won this game"<< "\n";
                            return 0;
                        }
                        
                    }
                }

        }
        std::cout << ""<< "\n";
        if(player1_start_cards.size() == 0)
        {
            std::cout << "player2 won this game"<< "\n";
            return 0;
        }
        if(player2_start_cards.size() == 0)
        {
            std::cout << "player1 won this game"<< "\n";
            return 0;
        }

    }

}