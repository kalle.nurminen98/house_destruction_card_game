# House destruction card game

Card game where 2 players shows top card of the deck and the player which has higher card wins other player card. Game ends 1 player has all cards. 

If both players cards are the same then happens battle and both players put next 3 cards on the line and 5 card of the deck checks which player wins all those cards.

If again card values are same then deck is shuffled and then players draw cards again.

If other player deck size is smaller than 5 then lower deck size player put all cards on the line and last card battles and other player puts same amount of cards on the line.

## Requirements

requires gcc and cmake

## Usage

$ mkdir build && cd build

$ cmake ../

$ make

$ ./main

## Restriction

There is small restriction which occurs after battle happens and one of the players deck size is under 5 because vector checks it and terminates the program after that

## Maintainers

Kalle Nurminen 
